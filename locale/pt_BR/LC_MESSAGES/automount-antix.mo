��          �      �       H     I     `     p     v     �     �  &   �  "   �     �  ?   �  5   <     r     �  �  �     r     �     �  '   �  /   �     �  X     3   \     �  b   �  a   �     X     h                  	      
                                             Audio Disc Command     DVD Command Apply Automount Configuration Automount External Devices Close Do Not Change zzzfm automount settings Open Default File Manager on Mount \n auto-launch preferred Music Player upon insertion of Audio Disc auto-launch preferred Video Player upon DVD insertion optical discs usb devices Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-05-31 12:45+0000
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2020-2021,2023
Language-Team: Portuguese (Brazil) (http://www.transifex.com/anticapitalista/antix-development/language/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Comando do Disco de Áudio Comando do DVD Aplicar Configurações de Montagem Automática Montar Automaticamente os Dispositivos Externos Fechar Não alterar as configurações de montagem automática do gerenciador de arquivos zzzFM Abrir o Gerenciador de Arquivos Padrão na Montagem \n Iniciar automaticamente o reprodutor de áudio preferido após inserir uma mídia de CD de música Iniciar automaticamente o reprodutor de vídeo preferido após inserir uma mídia de DVD de filme discos ópticos dispositivos usb 