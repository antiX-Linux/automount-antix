��          �      �       H     I     `     p     v     �     �  (   �  "   �     �  ?   �  5   >     t     �  �  �       
   .     9     B  (   `     �  8   �  >   �       K   
  F   V     �     �           	      
                                                    Audio Disc Command     DVD Command Apply Automount Configuration Automount External Devices Close Do Not Change Spacefm automount settings Open Default File Manager on Mount \n auto-launch preferred Music Player upon insertion of Audio Disc auto-launch preferred Video Player upon DVD insertion optical discs usb devices Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-05-31 12:45+0000
Last-Translator: asd asd <ekeimaja@gmail.com>, 2021
Language-Team: Finnish (http://www.transifex.com/anticapitalista/antix-development/language/fi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fi
Plural-Forms: nplurals=2; plural=(n != 1);
 Äänilevyn käsky DVD-käsky Hyväksy Automaattiliitoksen asetukset Liitä ulkoiset laitteet automaattisesti Sulje Älä muuta Spacefm automaattisen liittämisen asetuksia Avaa oletukseksi määritetty tiedostonhallinta liitettäessä \n Käynnistä automaattisesti musiikkisoitin äänilevyn asettamisen jälkeen Käynnistä automaattisesti videosoitin DVD-levyn asettamisen jälkeen optiset levyt USB-laitteet 