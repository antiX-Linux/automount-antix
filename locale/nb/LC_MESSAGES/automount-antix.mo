��          �      �       H     I     `     p     v     �     �  &   �  "   �     �  ?   �  5   <     r     �  �  �     ,     @     M     R     k     �  1   �  )   �     �  P   �  P   <     �     �                  	      
                                             Audio Disc Command     DVD Command Apply Automount Configuration Automount External Devices Close Do Not Change zzzfm automount settings Open Default File Manager on Mount \n auto-launch preferred Music Player upon insertion of Audio Disc auto-launch preferred Video Player upon DVD insertion optical discs usb devices Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-05-31 12:45+0000
Last-Translator: heskjestad <cato@heskjestad.xyz>, 2021,2023
Language-Team: Norwegian Bokmål (http://www.transifex.com/anticapitalista/antix-development/language/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
 Kommando for lyd-CD DVD-kommando Bruk Oppsett av automontering Automonter eksterne enheter Lukk Ikke endre automonterings-innstillinger for zzzfm Åpne forvalgt filbehandler ved montering \n foretrukket lydavspiller som skal starte automatisk ved innsetting av lyd-disker foretrukket videospiller som skal starte automatisk ved innsetting av DVD-plater optiske plater usb-enheter 