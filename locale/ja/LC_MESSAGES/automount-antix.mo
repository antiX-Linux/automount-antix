��          �      �       H     I     `     p     v     �     �  &   �  "   �     �  ?   �  5   <     r     �  t  �  ,        .     C     J  '   f  	   �  3   �  Q   �       Z   !  F   |     �     �                  	      
                                             Audio Disc Command     DVD Command Apply Automount Configuration Automount External Devices Close Do Not Change zzzfm automount settings Open Default File Manager on Mount \n auto-launch preferred Music Player upon insertion of Audio Disc auto-launch preferred Video Player upon DVD insertion optical discs usb devices Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-05-31 12:45+0000
Last-Translator: Green, 2021,2023
Language-Team: Japanese (http://www.transifex.com/anticapitalista/antix-development/language/ja/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ja
Plural-Forms: nplurals=1; plural=0;
     オーディオディスク コマンド     DVD コマンド 適用 自動マウントの設定 外部デバイスの自動マウント 閉じる zzzfm の自動マウント設定を変更しない マウント時にデフォルトのファイルマネージャーを開きます \n オーディオディスク挿入時に任意の音楽プレーヤーを自動起動する DVD 挿入時に任意のビデオプレーヤーを自動起動する 光学ディスク USB デバイス 