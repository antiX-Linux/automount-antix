��          �      �       H     I     `     p     v     �     �  &   �  "   �     �  ?   �  5   <     r     �  �  �     *     B     S     [     r     �  +   �  7   �     �  I   �  C   I     �     �                  	      
                                             Audio Disc Command     DVD Command Apply Automount Configuration Automount External Devices Close Do Not Change zzzfm automount settings Open Default File Manager on Mount \n auto-launch preferred Music Player upon insertion of Audio Disc auto-launch preferred Video Player upon DVD insertion optical discs usb devices Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-05-31 12:45+0000
Last-Translator: Besnik Bleta <besnik@programeshqip.org>, 2021,2023
Language-Team: Albanian (http://www.transifex.com/anticapitalista/antix-development/language/sq/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sq
Plural-Forms: nplurals=2; plural=(n != 1);
 Urdhër për Disk Audio Urdhër për DVD Zbatoje Formësim Vetëmontimi Vetëmonto Pajisje të Jashtme Mbylle Mos Ndryshoni rregullime vetëmontimi zzzfm Gjatë Montimi, hap Përgjegjës Parazgjedhje Kartelash \n gjatë futjesh CD-sh Audio vetënis Lojtësin e parapëlqyer të Muzikës gjatë futjesh DVD-je vetënis Lojtësin e parapëlqyer të Videove disqe optikë pajisje usb 