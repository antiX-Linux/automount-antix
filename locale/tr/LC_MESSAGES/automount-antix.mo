��          �      �       H     I     `     p     v     �     �  (   �  "   �     �  ?   �  5   >     t     �  �  �     (  
   9     D  #   K  "   o     �  7   �  9   �     
  J     H   X     �     �           	      
                                                    Audio Disc Command     DVD Command Apply Automount Configuration Automount External Devices Close Do Not Change Spacefm automount settings Open Default File Manager on Mount \n auto-launch preferred Music Player upon insertion of Audio Disc auto-launch preferred Video Player upon DVD insertion optical discs usb devices Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-05-31 12:45+0000
Last-Translator: mahmut özcan <mahmutozcan@protonmail.com>, 2021
Language-Team: Turkish (http://www.transifex.com/anticapitalista/antix-development/language/tr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: tr
Plural-Forms: nplurals=2; plural=(n > 1);
 Ses Diski Komutu DVD Komutu Uygula Otomatik Bağlama Yapılandırması Harici Aygıtları Otomatik Bağla Kapat Spacefm otomatik bağlama ayarlarını Değiştirmeyin  Bağlandığında Öntanımlı Dosya Yöneticisi İle Aç \n Ses Diski takıldığında tercih edilen Müzik Çaları otomatik başlat  DVD takıldığında tercih edilen Video Oynatıcıyı otomatik başlat  optik diskler usb aygıtları 