��          �      �       H     I     `     p     v     �     �  (   �  "   �     �  ?   �  5   >     t     �  v  �               !  $   -  )   R  	   |  C   �  6   �       J     H   O     �     �           	      
                                                    Audio Disc Command     DVD Command Apply Automount Configuration Automount External Devices Close Do Not Change Spacefm automount settings Open Default File Manager on Mount \n auto-launch preferred Music Player upon insertion of Audio Disc auto-launch preferred Video Player upon DVD insertion optical discs usb devices Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-05-31 12:45+0000
Last-Translator: Feri, 2022
Language-Team: Hungarian (http://www.transifex.com/anticapitalista/antix-development/language/hu/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hu
Plural-Forms: nplurals=2; plural=(n != 1);
 Hang CD parancs DVD parancs Alkalmazás Automatikus csatolás beállítások Külső eszközök automatikus csatolása Bezárás Ne változtassa meg a Spacefm automatikus csatolás beállításait Alapértelmezett fájlkezelő megnyitása csatoláskor \n a preferált zenelejátszó automatikus indítása hang CD behelyezésekor a preferált videólejátszó automatikus indítása DVD behelyezésekor optikai lemezek usb eszközök 