��          �      �       H     I     `     p     v     �     �  &   �  "   �     �  ?   �  5   <     r     �  �  �     A     X     d     l      �     �  <   �  3   �     #  J   &  =   q     �     �                  	      
                                             Audio Disc Command     DVD Command Apply Automount Configuration Automount External Devices Close Do Not Change zzzfm automount settings Open Default File Manager on Mount \n auto-launch preferred Music Player upon insertion of Audio Disc auto-launch preferred Video Player upon DVD insertion optical discs usb devices Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-05-31 12:45+0000
Last-Translator: Manuel <senpai99@hotmail.com>, 2023
Language-Team: Spanish (http://www.transifex.com/anticapitalista/antix-development/language/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Comando disco de audio Comando DVD Aplicar Configuración de Automontaje Automontar Dispositivos Externos Cerrar No cambiar la configuración de montaje automático de zzzfm Abrir el gestor de archivos predeterminado en mount \n auto-lanzar reproductor de música preferido al insertar un disco de audio auto-lanzar reproductor de video preferido al insertar un DVD discos ópticos dispositivos usb 