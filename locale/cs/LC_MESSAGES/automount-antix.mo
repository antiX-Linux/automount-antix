��          �      �       H     I     `     p     v     �     �  (   �  "   �     �  ?   �  5   >     t     �  �  �     k     �     �  %   �  /   �     �  A   �  :   >     y  M   |  K   �          %           	      
                                                    Audio Disc Command     DVD Command Apply Automount Configuration Automount External Devices Close Do Not Change Spacefm automount settings Open Default File Manager on Mount \n auto-launch preferred Music Player upon insertion of Audio Disc auto-launch preferred Video Player upon DVD insertion optical discs usb devices Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-05-31 12:45+0000
Last-Translator: joyinko <joyinko@azet.sk>, 2021,2023
Language-Team: Czech (http://www.transifex.com/anticapitalista/antix-development/language/cs/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cs
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;
     Audio Disc příkaz     DVD příkaz Použít Nastavení automatického připojení Automatické připojení externích zařízení Zavřít Neměň nastavení automatického připojování programu Spacefm Po připojení otevřít předvolený prohlízeč souborů \n automaticky spusť preferovaný přehrávač hudby při vložení audio disku automaticky spusť preferovaný přehrávač videa při vložení DVD disku optické disky usb zařízení 