��          �      �       H     I     `     p     v     �     �  &   �  "   �     �  ?   �  5   <     r     �  �  �  "   ,     O     `     q  /   �     �  a   �  c   3     �  �   �  �   V     �                       	      
                                             Audio Disc Command     DVD Command Apply Automount Configuration Automount External Devices Close Do Not Change zzzfm automount settings Open Default File Manager on Mount \n auto-launch preferred Music Player upon insertion of Audio Disc auto-launch preferred Video Player upon DVD insertion optical discs usb devices Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-05-31 12:45+0000
Last-Translator: anticapitalista <anticapitalista@riseup.net>, 2021,2023
Language-Team: Greek (http://www.transifex.com/anticapitalista/antix-development/language/el/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: el
Plural-Forms: nplurals=2; plural=(n != 1);
 Εντολή δίσκου ήχου Εντολή DVD Εφαρμογή Διαμόρφωση Automount Automount εξωτερικές συσκευές Κλείσιμο Μην αλλάξετε τις ρυθμίσεις αυτόματου λογαριασμού zzzFM Ανοίξτε το προεπιλεγμένος διαχειριστής αρχείων σε Mount \n αυτόματη εκκίνηση προτιμώμενου προγράμματος αναπαραγωγής μουσικής κατά την εισαγωγή του δίσκου ήχου αυτόματη εκκίνηση προτιμώμενου προγράμματος αναπαραγωγής βίντεο κατά την εισαγωγή DVD οπτικούς δίσκους συσκευές USB 