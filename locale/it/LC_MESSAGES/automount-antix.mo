��          �      �       H     I     `     p     v     �     �  &   �  "   �     �  ?   �  5   <     r     �  �  �     K     _     k     s  !   �     �  =   �  ,   �     %  U   (  L   ~     �     �                  	      
                                             Audio Disc Command     DVD Command Apply Automount Configuration Automount External Devices Close Do Not Change zzzfm automount settings Open Default File Manager on Mount \n auto-launch preferred Music Player upon insertion of Audio Disc auto-launch preferred Video Player upon DVD insertion optical discs usb devices Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-05-31 12:45+0000
Last-Translator: Dede Carli <dede.carli.drums@gmail.com>, 2023
Language-Team: Italian (http://www.transifex.com/anticapitalista/antix-development/language/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Comando disco audio Comando DVD Applica Configurazione dell'Automount Automount dei Dispositivi Esterni Chiudi Non cambiare le impostazioni di montaggio automatico di zzzfm Apri il File Manager di Default al Montaggio \n avvio automatico del riproduttore preferito di musica all'inserimento del disco audio avvio automatico del riproduttore preferito di video all'inserimento del DVD dischi ottici dispositivi usb 