��          �      �       H     I     `     p     v     �     �  &   �  "   �     �  ?   �  5   <     r     �  �  �     L     i  	   z  $   �  0   �     �  ?   �  I   !     k  \   n  O   �          ,                  	      
                                             Audio Disc Command     DVD Command Apply Automount Configuration Automount External Devices Close Do Not Change zzzfm automount settings Open Default File Manager on Mount \n auto-launch preferred Music Player upon insertion of Audio Disc auto-launch preferred Video Player upon DVD insertion optical discs usb devices Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-05-31 12:45+0000
Last-Translator: Wallon Wallon, 2023
Language-Team: French (Belgium) (http://app.transifex.com/anticapitalista/antix-development/language/fr_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr_BE
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
     Commande du disque audio     Commande DVD Appliquer Configuration du montage automatique Montage automatique des périphériques externes Fermer Ne pas modifier les paramètres de montage automatique de zzzfm Lancer le gestionnaire de fichiers par défaut à l’occasion de montage \n lancement automatique du lecteur de musique préféré lors de l’insertion du disque audio lancement automatique du lecteur vidéo préféré lors de l’insertion du DVD disques optiques les périphériques usb 