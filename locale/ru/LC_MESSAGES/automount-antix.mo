��          �      �       H     I     `     p     v     �     �  (   �  "   �     �  ?   �  5   >     t     �    �     �     �     �  3   �  B   $     g  T   v  l   �     8  s   ;  e   �  !        7           	      
                                                    Audio Disc Command     DVD Command Apply Automount Configuration Automount External Devices Close Do Not Change Spacefm automount settings Open Default File Manager on Mount \n auto-launch preferred Music Player upon insertion of Audio Disc auto-launch preferred Video Player upon DVD insertion optical discs usb devices Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-05-31 12:45+0000
Last-Translator: Андрей Холманских, 2022
Language-Team: Russian (http://www.transifex.com/anticapitalista/antix-development/language/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Команда для CD Команда для DVD Применить Настройка автомонтирования Автомонтирование внешних устройств Закрыть Не изменять настройки автомонтирования в Spacefm Открывать файловый менеджер по умолчанию при монтировании \n автозапуск выбранного музыкального плеера при вставке аудио-CD автозапуск выбранного видеоплеера при вставке DVD-диска оптических дисков usb-устройства 