��          �      �       H     I     `     p     v     �     �  &   �  "   �     �  ?   �  5   <     r     �  �  �     b     u     ~  "   �  "   �     �  2   �  &        +  G   .  F   v     �     �                  	      
                                             Audio Disc Command     DVD Command Apply Automount Configuration Automount External Devices Close Do Not Change zzzfm automount settings Open Default File Manager on Mount \n auto-launch preferred Music Player upon insertion of Audio Disc auto-launch preferred Video Player upon DVD insertion optical discs usb devices Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-05-31 12:45+0000
Last-Translator: Arnold Marko <arnold.marko@gmail.com>, 2019,2021,2023
Language-Team: Slovenian (http://www.transifex.com/anticapitalista/antix-development/language/sl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
 Ukaz za zvočni CD DVD ukaz Potrdi Konfiguracija za samodejni priklop Samodejno priklopi zunanje naprave Zapri Ne spreminjaj nastavitev za zzfm samodejni priklop Odpri upravljalnik datotek na priklopu \n po vstavitvi zvočnega CD samodejno zaženi privzeti video predvajalnik ob vstavitvi DVD nosilca samodejno zaženi privzeti video predvajalnik optični diski usb naprave 