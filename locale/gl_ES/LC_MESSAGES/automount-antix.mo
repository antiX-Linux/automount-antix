��          �      �       H     I     `     p     v     �     �  (   �  "   �     �  ?   �  5   >     t     �  �  �     @     Z     i  '   q  .   �     �  =   �  7        E  U   H  Q   �     �                 	      
                                                    Audio Disc Command     DVD Command Apply Automount Configuration Automount External Devices Close Do Not Change Spacefm automount settings Open Default File Manager on Mount \n auto-launch preferred Music Player upon insertion of Audio Disc auto-launch preferred Video Player upon DVD insertion optical discs usb devices Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-05-31 12:45+0000
Last-Translator: David Rebolo Magariños <drgaga345@gmail.com>, 2020-2021
Language-Team: Galician (Spain) (http://www.transifex.com/anticapitalista/antix-development/language/gl_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gl_ES
Plural-Forms: nplurals=2; plural=(n != 1);
 Comando do Disco de Audio Comando do DVD Aplicar Configuración da conexión automática Conectar automaticamente dispositivos externos Cerrar Non cambie a configuración da montaxe automática de Spacefm Ao conectar, abrir o Xestor de Ficheiros predeterminado \n iniciar automaticamente o reprodutor de música preferido ao inserir o disco de audio iniciar automaticamente o reprodutor de vídeo preferido tras a inserción do DVD discos ópticos dispositivos usb 