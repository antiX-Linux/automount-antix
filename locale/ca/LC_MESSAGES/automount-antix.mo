��          �      �       H     I     `     p     v     �     �  &   �  "   �     �  ?   �  5   <     r     �  �  �     %     2     @     G  $   e     �  ,   �  1   �     �  G   �  I   ;     �     �                  	      
                                             Audio Disc Command     DVD Command Apply Automount Configuration Automount External Devices Close Do Not Change zzzfm automount settings Open Default File Manager on Mount \n auto-launch preferred Music Player upon insertion of Audio Disc auto-launch preferred Video Player upon DVD insertion optical discs usb devices Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-05-31 12:45+0000
Last-Translator: Eduard Selma <selma@tinet.cat>, 2016,2021,2023
Language-Team: Catalan (http://www.transifex.com/anticapitalista/antix-development/language/ca/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ca
Plural-Forms: nplurals=2; plural=(n != 1);
 Ordre pel CD Ordre pel DVD Aplica Configuració d'automuntatge  Automuntatge de dispositius externs  Tanca  No canvia els paràmetres de zzzfm automount Obre el gestor de fitxers per omissió en muntar  \n engegada automàtica del reproductor d'àudio preferit en inserir un CD engegada automàtica del reproductor de vídeo preferit en inserir un DVD discos òptics  dispositius USB  