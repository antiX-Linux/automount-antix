��          �      �       H     I     `     p     v     �     �  &   �  "   �     �  ?   �  5   <     r     �  �  �     ;     P     \  &   d  ,   �     �  3   �  4   �     (  Q   +  A   }     �     �                  	      
                                             Audio Disc Command     DVD Command Apply Automount Configuration Automount External Devices Close Do Not Change zzzfm automount settings Open Default File Manager on Mount \n auto-launch preferred Music Player upon insertion of Audio Disc auto-launch preferred Video Player upon DVD insertion optical discs usb devices Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-05-31 12:45+0000
Last-Translator: Paulo C., 2023
Language-Team: Portuguese (http://www.transifex.com/anticapitalista/antix-development/language/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Comando Disco Áudio Comando DVD Aplicar Configuração da montagem automática Montar automaticamente dispositivos externos Fechar Não alterar definições de auto-montagem do zzzfm Ao montar, abrir o Gestor de Ficheiros Pré-definido \n iniciar automaticamente o Leitor de Áudio padrão ao inserir um Disco de Áudio. iniciar automaticamente o Leitor de Vídeo padrão ao inserir DVD discos ópticos dispositivos usb 