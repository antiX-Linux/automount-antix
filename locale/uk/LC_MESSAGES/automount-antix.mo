��          �      �       H     I     `     p     v     �     �  (   �  "   �     �  ?   �  5   >     t     �  x  �  '        /     F  5   ]  B   �     �  V   �  c   <     �  o   �  ^        r     �           	      
                                                    Audio Disc Command     DVD Command Apply Automount Configuration Automount External Devices Close Do Not Change Spacefm automount settings Open Default File Manager on Mount \n auto-launch preferred Music Player upon insertion of Audio Disc auto-launch preferred Video Player upon DVD insertion optical discs usb devices Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-05-31 12:45+0000
Last-Translator: Tymofii Lytvynenko <till.svit@gmail.com>, 2022
Language-Team: Ukrainian (http://www.transifex.com/anticapitalista/antix-development/language/uk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: uk
Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n % 10 == 1 && n % 100 != 11 ? 0 : n % 1 == 0 && n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14) ? 1 : n % 1 == 0 && (n % 10 ==0 || (n % 10 >=5 && n % 10 <=9) || (n % 100 >=11 && n % 100 <=14 )) ? 2: 3);
     Команда аудіодиска     Команда DVD Застосувати Конфігурація автомонтування Автомонтування зовнішніх пристроїв Закрити Не змінювати налаштування автомонтування Spacefm  Відкривати типовий файловий менеджер при змонтуванні \n Автозапуск обраного аудіопрогравача при вставці аудіодиска Автозапуск обраного відеопрогравача при вставці DVD Оптичні диски Пристрої USB 