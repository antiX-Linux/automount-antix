��          �      �       H     I     `     p     v     �     �  (   �  "   �     �  ?   �  5   >     t     �  �  �          8  	   I  $   S  )   x     �  /   �  .   �     	  B     8   O     �     �           	      
                                                    Audio Disc Command     DVD Command Apply Automount Configuration Automount External Devices Close Do Not Change Spacefm automount settings Open Default File Manager on Mount \n auto-launch preferred Music Player upon insertion of Audio Disc auto-launch preferred Video Player upon DVD insertion optical discs usb devices Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-05-31 12:45+0000
Last-Translator: Vanhoorne Michael, 2022
Language-Team: Dutch (Belgium) (http://www.transifex.com/anticapitalista/antix-development/language/nl_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nl_BE
Plural-Forms: nplurals=2; plural=(n != 1);
     Audio Schijf Commando     DVD Commando Toepassen Automatisch Aankoppelen Configuratie Automatisch Externe Apparaten Aankoppelen Sluiten Verander de Spacefm automount instellingen niet Open Standaard Bestandsmanager bij Aankoppelen \n auto-start voorkeur Muziekspeler bij plaatsen van een Audio Schijf auto-start voorkeur Videospeler bij plaatsen van een DVD optische schijven usb apparaten 