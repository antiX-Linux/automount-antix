��          �      �       H     I     `     p     v     �     �  (   �  "   �     �  ?   �  5   >     t     �  t  �               (     1  +   I  
   u  l   �  ,   �       L     `   j     �     �           	      
                                                    Audio Disc Command     DVD Command Apply Automount Configuration Automount External Devices Close Do Not Change Spacefm automount settings Open Default File Manager on Mount \n auto-launch preferred Music Player upon insertion of Audio Disc auto-launch preferred Video Player upon DVD insertion optical discs usb devices Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-05-31 12:45+0000
Last-Translator: Robin, 2021
Language-Team: German (http://www.transifex.com/anticapitalista/antix-development/language/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
 Befehl für Audio-CD Befehl für DVD Anwenden Automount-Konfiguration Datenträger automatisch einhängen (mount) Schließen Ändere die Einstellungen für das automatische Einhängen von Laufwerken des Programms “SpaceFM” nicht. Standard-Fenstermanager beim Mounten öffnen \n Starte das voreingestellte Audioabspielprogramm beim Einlegen einer Audio-CD Starte das für das Abspielen von Videos voreingestellte Abspielprogramm beim Einlegen einer DVD optische Platten USB-Geräte 