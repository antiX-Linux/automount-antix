��          �      �       H     I     `     p     v     �     �  &   �  "   �     �  ?   �  5   <     r     �  �  �     &     9  	   J     T     l     �  *   �  *   �     �  E   �  =   ,     j     y                  	      
                                             Audio Disc Command     DVD Command Apply Automount Configuration Automount External Devices Close Do Not Change zzzfm automount settings Open Default File Manager on Mount \n auto-launch preferred Music Player upon insertion of Audio Disc auto-launch preferred Video Player upon DVD insertion optical discs usb devices Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-05-31 12:45+0000
Last-Translator: Henry Oquist <henryoquist@nomalm.se>, 2021,2023
Language-Team: Swedish (http://www.transifex.com/anticapitalista/antix-development/language/sv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sv
Plural-Forms: nplurals=2; plural=(n != 1);
 Ljudskiva Kommando     DVD Kommando Genomför Automount Konfiguration Automount Externa Enheter Stäng Ändra inte zzzfm automount-inställningar Öppna Standard Filhanterare vid Montering \n automatstarta föredragen Musikspelare vid insättning av Ljud-skiva  automatstarta föredragen Videospelare vid insättning av DVD optiska skivor usb-enheter 